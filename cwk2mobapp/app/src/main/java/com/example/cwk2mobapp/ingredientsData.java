package com.example.cwk2mobapp;

import static android.provider.BaseColumns._ID ;
import static com.example.cwk2mobapp.Constants.NAMEOFPRODUCT;
import static com.example.cwk2mobapp.Constants.TABLE_NAME_INGREDIENTS;
import static com.example.cwk2mobapp.Constants.WEIGHT;
import static com.example.cwk2mobapp.Constants.PRICE;
import static com.example.cwk2mobapp.Constants.DESCRIPTION;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ingredientsData extends SQLiteOpenHelper {
    private static final String TAG = "ingredient";

    private static final String DATABASE_NAME = "ingredients.db";
    private static final int DATABASE_VERSION = 1;




    public ingredientsData(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        String createTable = " CREATE TABLE IF NOT EXISTS " + TABLE_NAME_INGREDIENTS + "("
                + _ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + NAMEOFPRODUCT + " TEXT NOT NULL,"
                + WEIGHT + " DOUBLE, "
                + PRICE + " DOUBLE, "
                + DESCRIPTION + " TEXT NOT NULL);";
        db.execSQL(createTable);
        System.out.println("CREATE THIS TABLE    "+ createTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS " + TABLE_NAME_INGREDIENTS);
        onCreate(db);

    }

    public Boolean saveData(String name, Double weight, Double price, String description) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NAMEOFPRODUCT, name);
        contentValues.put(WEIGHT, weight);
        contentValues.put(PRICE, price);
        contentValues.put(DESCRIPTION, description);

        Log.d(TAG, "ADD DATA: ADDING " + name + "to " + TABLE_NAME_INGREDIENTS);
        long result = db.insert(TABLE_NAME_INGREDIENTS, null, contentValues);

        if (result == -1) {
            System.out.println(" false");

            return false;
        } else {
            System.out.println(" true");

            return true;
        }
    }

}
